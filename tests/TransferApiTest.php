<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use Transfer\Requests\BookingRequest;
use Transfer\Transfer;


class TransferApiTest extends TestCase
{
    public $repository;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        \Dotenv\Dotenv::create(__DIR__,'../.env')->load();

        $this->repository = new Transfer(getenv('HOTELBEDS_URL'), getenv('HOTELBEDS_KEY'),getenv('HOTELBEDS_SECRET'));
    }

    /**
    * @test
    */
    public function it_will_search_for_available_transfer_vehicle(){

       $availability = $this->getAvailability();

        $this->assertArrayHasKey('services', (array) $availability);

        $this->assertNotCount(0, $availability->services);
    }

    /**
    * @test
    */
    public function it_will_book_transfer(){

        $availability = $this->getAvailability();

        $booking  = $this->bookTransfer($availability->services[1]);

        $this->assertArrayHasKey('bookings', (array) $booking);
    }
    
    /**
    * @test
    */
    public function it_will_get_transfer_details(){

        $availability = $this->getAvailability();

        $response  = $this->bookTransfer($availability->services[1]);

        $detail = $this->repository->bookingDetail($response->bookings[0]->reference);

        $this->assertArrayHasKey('bookings', (array) $detail);

    }
    
    /**
    * @test
    */
    public function it_will_cancel_transfer_booking(){

        $availability = $this->getAvailability();

        $response  = $this->bookTransfer($availability->services[1]);

        $cancel = $this->repository->bookingCancellation($response->bookings[0]->reference);

        $this->assertArrayHasKey('bookings', (array) $cancel);
    }

    private function getAvailability(){

        $request = [
            'from'        => [
                'type'  => 'IATA',
                'code'  => 'MNL'
            ],
            'to'      => [
                'type'    => 'ATLAS',
                'code'    => '85248'
            ],
            'outbound'  => date('Y-m-d\TH:i:s', strtotime("+9 day")),
            'adults'    => 2,
            'children'  => 0,
            'infants'   => 0
        ];

        $url = "/from/".$request['from']['type']."/".$request['from']['code'];
        $url .= "/to/".$request['to']['type']."/".$request['to']['code'];
        $url .= "/".$request['outbound']."/".$request['adults']."/".$request['children']."/".$request['infants'];

        return $this->repository->availability($url);
    }

    private function bookTransfer($availability) {

        $detail = [
            'holder'   => [
                'title'     => 'Mr',
                'name'      => 'Alvin',
                'surname'   => 'Cacayurin',
                'email'     => 'alvincacayurin1014@gmail.com',
                'phone'     => '09198606294',
                'type'      => 'ADULT',
                'age'       => 25,
            ],
            'transfers' => [
                0 => [
                    'rateKey' => $availability->rateKey,
                    'detail'  => [
                        'departureFlightNumber' => 'ASDFGHJ',
                        'departureShipName'     => '',
                        'departureTrainInfo'    => [
                            'trainCompanyName'  => '',
                            'trainNumber'       => '',
                        ],
                        'arrivalFlightNumber'   => 'ASDFGHJ',
                        'arrivalShipName'       => '',
                        'arrivalTrainInfo'      => [
                            'trainCompanyName'  => '',
                            'trainNumber'       => ''
                        ],

                    ]
                ]
            ],
            'language'  => 'en',
            'welcomeMessage'    => '',
            'remarks'           => ''
        ];

        $request = new BookingRequest();

        $request->setHolder($detail['holder'])
            ->setTransfers($detail['transfers']);

        $booking = $this->repository->booking($request);

        return $booking;

    }
}