<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 31/05/2019
 * Time: 1:52 PM
 */

namespace Transfer\Requests;


class BookingRequest extends TransferRequest
{
    protected $holder = [];
    protected $transfers = [];
    protected $clientReference;
    protected $language;

    public function __construct()
    {
        $this->clientReference = 'rainalbert';
        $this->language = 'en';
    }

    public function setHolder($holder) {

        $this->holder = $holder;

        return $this;
    }

    public function setTransfers($transfers) {

        $this->transfers = $transfers;

        return $this;
    }
}