<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 31/05/2019
 * Time: 1:53 PM
 */

namespace Transfer\Requests;


class TransferRequest
{
    public function get()
    {
        return get_object_vars($this);
    }
}