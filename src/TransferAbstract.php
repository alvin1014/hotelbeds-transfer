<?php

namespace Transfer;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use HttpException;
use Psr\Http\Message\ResponseInterface;
use Transfer\Exceptions\TransferException;
use Transfer\Requests\BookingRequest;

abstract class TransferAbstract {

    protected $endpoint;
    protected $key;
    protected $secret;

    /**
     * @param $method
     * @param $url
     * @param array $request
     * @return mixed|ResponseInterface
     * @throws HttpException
     * @throws TransferException
     */
    protected function send($method, $url, $request = []) {

        $client = $this->createClient();

        try {

            $body = [];

            if(!empty($request)){
                $body = ['body' => json_encode($request)];
            }

            $response = $client->request($method, $this->endpoint.$url, $body);

        }catch(BadResponseException $e) {

            $exception = json_decode($e->getResponse()->getBody()->getContents());

            return $exception;

        }

        return $this->parseResponse($response);

    }

    private function createClient(){

        return new Client([
            'headers'   => [
                'Accept-Encoding'       => 'gzip',
                'Content-Type'          => 'application/json',
                'Accept'                => 'application/json',
                'Api-Key'               => $this->key,
                'X-Signature'           => $this->buildSignature()
            ]
        ]);

    }

    private function buildSignature(){

        return hash('sha256', $this->key.$this->secret.time());

    }

    /**
     * @param ResponseInterface $response
     * @return mixed|ResponseInterface
     * @throws HttpException
     * @throws TransferException
     */
    private function parseResponse(ResponseInterface $response) {

        $response = $this->toArray($response);

        if(isset($response->error)) {
            throw new TransferException($response->error->message, $response->error->code);
        }

        return $response;

    }

    private function toArray(ResponseInterface $response)
    {
        return json_decode($response->getBody()->getContents());
    }

    protected function buildRequest($request)
    {
        return http_build_query($request);
    }

}