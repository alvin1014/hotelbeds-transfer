<?php

namespace Transfer;


use Transfer\Requests\BookingRequest;

class Transfer extends TransferAbstract implements TransferContract
{
    /**
     * Transfer constructor.
     * @param $endpoint
     * @param $key
     * @param $secret
     */
    public function __construct($endpoint, $key, $secret)
    {
        $this->endpoint = $endpoint;
        $this->key = $key;
        $this->secret = $secret;
    }

    /**
     * @param $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws Exceptions\TransferException
     * @throws \HttpException
     */
    public function availability($request)
    {
        return $this->send('GET','/transfer-api/1.0/availability/en'.$request);
    }

    /**
     * @param BookingRequest $request
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws Exceptions\TransferException
     * @throws \HttpException
     */
    public function booking(BookingRequest $request)
    {
        return $this->send('POST','/transfer-api/1.0/booking', $request->get());
    }

    /**
     * @param $reference
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws Exceptions\TransferException
     * @throws \HttpException
     */
    public function bookingDetail($request)
    {
        return $this->send('GET','/transfer-api/1.0/booking/en/reference/'.$request['reference']);
    }

    /**
     * @param $reference
     * @param string $simulation
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws Exceptions\TransferException
     * @throws \HttpException
     */
    public function bookingCancellation($request)
    {
        return $this->send('DELETE','/transfer-api/1.0/booking/en/reference/'.$request['reference']);
    }
}