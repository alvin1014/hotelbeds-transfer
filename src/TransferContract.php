<?php
/**
 * Created by PhpStorm.
 * User: Alvin
 * Date: 11/06/2019
 * Time: 4:08 PM
 */

namespace Transfer;

use Transfer\Requests\BookingRequest;

interface TransferContract
{
     public function availability($request);

     public function booking(BookingRequest $request);

     public function bookingDetail($request);

     public function bookingCancellation($request);
}